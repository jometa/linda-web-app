const _ = require('lodash');
const model = require('./model');
const map_priority = model.map_priority;
const map_kriteria = model.map_kriteria;

function _fsaw({ items, prios }) {
  const xprios = [
    map_priority(prios.c1),
    map_priority(prios.c2),
    map_priority(prios.c3),
    map_priority(prios.c4),
    map_priority(prios.c5),
    map_priority(prios.c6),
    map_priority(prios.c7)
  ];

  // Process weights
  const prioSummed = xprios.map(prio => prio.reduce((prev, v) => prev + v, 0));
  const prioTotal = prioSummed.reduce((prev, v) => prev + v, 0);
  const prioNormed = prioSummed.map(v => v / prioTotal);

  let xs = items.map(it => map_kriteria(it));

  let xsFuzz = xs.map(xrow => 
      xrow.map(xfuzz =>
        xfuzz.reduce((prev, v) => prev + v, 0) / 3.0
      )
  );

  let maxInColumns = xprios.map(i => -1);
  xsFuzz.forEach((it, i) => {
    it.forEach((v, j) => {
      if (v > maxInColumns[j]) {
        maxInColumns[j] = v;
      }
    });
  });

  let xsNorm = xsFuzz.map(xrow => 
    xrow.map((x, j) => 
      (x / maxInColumns[j])
    )
  );

  let vs = xsNorm.map(xrow => 
    xrow.reduce((acc, x, j) => acc + ( x * prioNormed[j] ), 0)
  );

  return vs.map((v, idx) => ({
    ...items[idx],
    v,
    vFormatted: model.format_v(v)
  }));
}

async function fsaw({ db }) {
  const candidateCollection = db.collection('candidates');
  const settingsCollection = db.collection('settings');

  const prios = await settingsCollection.findOne({
    key: 'priority'
  });
  let items = await candidateCollection.find();
  items = await items.toArray();

  return _fsaw({ items, prios });
}

async function sensitivity({ prios, db }) {
  const candidateCollection = db.collection('candidates');
  const settingsCollection = db.collection('settings');

  const originalPrios = await settingsCollection.findOne({
    key: 'priority'
  });

  let items = await candidateCollection.find();
  items = await items.toArray();

  const originalResult = _fsaw({ items, prios: originalPrios });
  const modifiedResult = _fsaw({ items, prios });

  const originalSorted = _.sortBy(originalResult, it => it.v);
  const modifiedSorted = _.sortBy(modifiedResult, it => it.v);

  const zipped = _.zip(originalSorted, modifiedSorted);
  const compareResult = zipped.map(([ a, b ]) => {
    const isSame = a._id == b._id;
    return { a, b, isSame };
  });

  const percentage = compareResult.filter(it => it.isSame).length * 100.0 / compareResult.length;

  return {
    compareResult,
    percentage
  };
}

module.exports = {
  fsaw,
  sensitivity
}
