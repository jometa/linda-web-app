const form_model = [
  {
    name: 'c1',
    label: 'Kehadiran',
    options: [
      { text: 'Rendah', value: 'r', fuzz: [0, 0, 0.5] },
      { text: 'Cukup', value: 'c', fuzz: [0, 0.5, 1.0] },
      { text: 'Baik', value: 'b', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c2',
    label: 'Kedisiplinan',
    options: [
      { text: 'Kurang Disiplin', value: 'k', fuzz: [0, 0, 0.5] },
      { text: 'Cukup Disiplin', value: 'c', fuzz: [0, 0.5, 1.0] },
      { text: 'Sangat Disiplin', value: 's', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c3',
    label: 'Prestasi Kerja',
    options: [
      { text: 'Sangat Rendah', value: 'st', fuzz: [0, 0, 0.25] },
      { text: 'Rendah', value: 'st', fuzz: [0, 0.25, 0.5] },
      { text: 'Cukup', value: 'c', fuzz: [0.25, 0.5, 0.75] },
      { text: 'Baik', value: 'b', fuzz: [0.5, 0.75, 1.0] },
      { text: 'Sangat Baik', value: 'sb', fuzz: [0.75, 1.0, 1.0] },
    ]
  },
  {
    name: 'c4',
    label: 'Perilaku',
    options: [
      { text: 'Kurang Memuaskan', value: 'km', fuzz: [0, 0, 0.5] },
      { text: 'Cukup Memuaskan', value: 'cm', fuzz: [0, 0.5, 1.0] },
      { text: 'Sangat Memuaskan', value: 'sm', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c5',
    label: 'Tanggung Jawab',
    options: [
      { text: 'Sangat Buruk', value: 'sb', fuzz: [0, 0, 0.5] },
      { text: 'Buruk', value: 'b', fuzz: [0, 0.5, 0.75] },
      { text: 'Cukup', value: 'c', fuzz: [0.5, 0.75, 1.0] },
      { text: 'Baik', value: 'ba', fuzz: [0.75, 1.0, 1.0] }
    ]
  },
  {
    name: 'c6',
    label: 'Kerja Sama Tim',
    options: [
      { text: 'Kurang Baik', value: 'kb', fuzz: [0, 0, 0.5] },
      { text: 'Cukup', value: 'c', fuzz: [0, 0.5, 1.0] },
      { text: 'Baik', value: 'ba', fuzz: [0.5, 1.0, 1.0] }
    ]
  },
  {
    name: 'c7',
    label: 'Keaktifan',
    options: [
      { text: 'Tidak Aktif', value: 'ta', fuzz: [0, 0, 0.5] },
      { text: 'Cukup Aktif', value: 'ca', fuzz: [0, 0.5, 1.0] },
      { text: 'Aktif', value: 'a', fuzz: [0.5, 1.0, 1.0] }
    ]
  }
];

const formatted = form_model.reduce((acc, model) => {
  acc[model.name] = s => {
    for (let opt of model.options) {
      if (opt.value == s) return opt.text;
    }
    throw new Error(`Can't find text for ${s} in kriteria ${model.label}`);
  };
  return acc;
}, {});

const priority_options = [
  { text: 'Sangat Rendah', value: 'sr' },
  { text: 'Rendah', value: 'r' },
  { text: 'Cukup', value: 'c' },
  { text: 'Tinggi', value: 't' },
  { text: 'Sangat Tinggi', value: 'st' }
];

function map_priority(s) {
  switch (s) {
    case 'sr': return [0, 0, 0.25];
    case 'r': return [0, 0.25, 0.5];
    case 'c': return [0.25, 0.5, 0.75];
    case 't': return [0.5, 0.75, 1.0];
    case 'st': return [0.75, 1.0, 1.0];
    default:
      throw new Error('Roar!!');
  }
}

function map_kriteria (item) {
  let results = [];
  for (let model of form_model) {
    let raw = item[model.name]
    let opt = model.options.find(({ value }) => value == raw)
    results.push(opt.fuzz);
  }
  return results;
}

function format_v(v) {
  if (v < 0.2) return 'Sangat Rendah';
  if (0.2 < v && v < 0.4) return 'Rendah';
  if (0.4 < v && v < 0.6) return 'Cukup';
  if (0.6 < v && v < 0.8) return 'Baik';
  if (0.8 < v) return 'Sangat Baik';
}

module.exports = {
  form_model,
  formatted,
  priority_options,
  map_priority,
  map_kriteria,
  format_v
};
