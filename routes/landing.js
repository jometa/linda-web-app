module.exports = async function (fastify, opts) {
  fastify.get('/', (request, reply) => {
    reply.view('landing/index.pug');
  });
};