const path = require('path');
const carbone = require('carbone');
const model = require('../../services/model');
const fsaw = require('../../services/fsaw');

const report_template_path = path.join(process.cwd(), 'reports/ranks-perangkingan.docx');

module.exports = async function(fastify, opts) {

  fastify.get('/', async function(request, reply) {
    let items = await fsaw.fsaw({ db: fastify.mongo.db });
    items = items.map(it => ({
      ...it,
      c1: model.formatted.c1(it.c1),
      c2: model.formatted.c2(it.c2),
      c3: model.formatted.c3(it.c3),
      c4: model.formatted.c4(it.c4),
      c5: model.formatted.c5(it.c5),
      c6: model.formatted.c6(it.c6),
      c7: model.formatted.c7(it.c7),
      v: it.v.toFixed(3)
    }));

    const total = items.length;
    const waktu = (new Date()).toLocaleString();
    const data = {
      total,
      waktu,
      items
    };

    carbone.render(report_template_path, data, function (err, result) {
      if (err) {
        console.log(err);
        reply.status(500).send('Error');
        return;
      } 
      reply
        .header('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        .send(result);
    });
  });

}