const fmulter = require('fastify-multer');
const upload = fmulter();
const model = require('../../services/model');

module.exports = async function (fastify, opts) {

  const collection = fastify.mongo.db.collection('settings');

  fastify.route({
    method: 'GET',
    url: '/',
    handler: async function (request, reply) {
      const result = await collection.findOne({ key: 'priority' });
      const { key, ...item } = result;
      reply.view('/admin/kriteria/index.pug', {
        title: 'Daftar Kriteria',
        subtitle: 'Menampilkan Daftar Kriteria secara lengkap',
        form_model: model.form_model,
        priority_options: model.priority_options,
        item: {}
      });
    }
  });

  fastify.route({
    method: 'POST',
    url: '/',
    preHandler: upload.none(),
    handler: async function (request, reply) {
      await collection.deleteOne({ key: 'priority' });
      await collection.insertOne({
        key: 'priority',
        ...request.body
      });
      reply.send('OK');
    }
  });

}