const fmulter = require('fastify-multer');
const upload = fmulter();
const model = require('../../services/model');
const fsaw = require('../../services/fsaw');

module.exports = async function (fastify, opts) {

  fastify.get('/', async (request, reply) => {
    const items = await fsaw.fsaw({ db: fastify.mongo.db });
    reply.view('admin/rank/index.pug', {
      title: 'Preangkingan Kandidat',
      subtitle: 'Menampilkan Perangkingan Kandidat dengan metode FSAW',
      items,
      form_model: model.form_model,
      formatted: model.formatted
    });
  });

};