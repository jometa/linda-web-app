const ObjectID = require('mongodb').ObjectID;
const fmulter = require('fastify-multer');
const upload = fmulter();
const model = require('../../services/model');

module.exports = async function (fastify, opts) {

  const collection = fastify.mongo.db.collection('candidates');

  fastify.route({
    url: '/',
    method: 'GET',
    handler: async (request, reply) => {
      let result = await collection.find();
      let items = await result.toArray();
      reply.view('admin/data/list.pug', {
        title: 'Daftar Kandidat',
        subtitle: 'Menampilkan Daftar Kandidat secara lengkap',
        items,
        form_model: model.form_model,
        formatted: model.formatted
      });
    }
  });

  fastify.route({
    url: '/create',
    method: 'GET',
    handler: async (request, reply) => {
      reply.view('admin/data/create.pug', {
        title: 'Tambah Kandidat',
        subtitle: 'Form Input Data Kandidat',
        form_model: model.form_model
      });
    }
  });

  fastify.route({
    url: '/create',
    method: 'POST',
    preHandler: upload.none(),
    handler: async (request, reply) => {
      const payload = request.body;
      const insertResult = await collection.insertOne(payload);
      reply.redirect('/admin/data/');
    }
  });

  fastify.route({
    url: '/:id',
    method: 'GET',
    handler: async (request, reply) => {
      const item = await collection.findOne({
        _id: new ObjectID(request.params.id)
      });

      reply.view('admin/data/edit.pug', {
        title: 'Tambah Kandidat',
        subtitle: 'Form Input Data Kandidat',
        form_model: model.form_model,
        formatted: model.formatted,
        item
      });
    }
  });

  fastify.route({
    url: '/delete/:id',
    method: 'GET',
    handler: async function (request, reply) {
      const id = request.params.id;
      await collection.deleteOne({
        _id: new ObjectID(id)
      });
      reply.redirect('/admin/data');
    }
  });

};