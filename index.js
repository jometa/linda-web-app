require('dotenv').config();

const fmongo = require('fastify-mongodb');
const fastify = require('fastify')();
const fcookie = require('fastify-cookie');
const fblipp = require('fastify-blipp');
const fstatic = require('fastify-static');
const fcors = require('fastify-cors');
const pov = require('point-of-view');
const path = require('path');
const pug = require('pug');
const fmulter = require('fastify-multer');

fastify.register(fcookie);
fastify.register(fblipp);
fastify.register(pov, {
	engine: {
		pug
	},
  templates: 'views',
  options: {
    basedir: path.join(__dirname, 'views')
  }
});
fastify.register(fcors);
fastify.register(fmulter.contentParser);
fastify.register(fmongo, {
  forceClose: true,
  url: process.env.MONGO_URL
});

fastify.register(require('./routes/landing'));
fastify.register(require('./routes/admin'), { prefix: 'admin' });
fastify.register(require('./routes/auth'), { prefix: 'auth' });

fastify.register(fstatic, {
	root: path.join(__dirname, 'resources'),
	prefix: '/resources/'
});

fastify.listen(process.env.PORT, function (err, address) {
	if (err) {
		console.log(err);
		exit(1);
	}
	console.log('listening at ' + address);
	fastify.blipp();
});
